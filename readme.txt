Information:
	This is the implementation of LISA method for Influence Maximization and Influence Spectrum problems with the diffusion process being either Independent Cascade(IC) or Linear Threshold(LT) model.
	
Compile:
    using make to compile
	make

Execute:
    
	./LISA -i <input file path> -o <output file path> -k <number of seeds> -p <approximation factor> -t <number of running threads> -m <propagation model> -s <linear or binary search> -h <1 or 0>

    Arguments:

        -i:
            path to the input binary network file (default: network.bin)

        -o:
            path to the expected output file (default: output.txt)

        -k:
            number of selected seed nodes (default: none)

        -p:
            approximation factor (default: 0.1)
	
	-t:
	    number of running threads (default: 1)
	
	-m:
	    diffusion model (LT or IC, default: LT)

	-s:
	    using linear or binary search when determining live edge in polling process (choose between 'linear' and 'binary', default: binary)

	-h:
	    using heuristics in choosing seed set or not (choose between 0 and 1, default: 0)

     Examples:
	Influence Maximization with k = 10:

		./LISA -i phy.bin -o phy.out -k 10 -p 0.1 -t 1 -m LT

	Influence Spectrum:
	
		./LISA -i phy.bin -o phy.out -p 0.1 -t 1 -m LT

     ***** Note: To run Influence Maximization, specify the number of selected seed nodes. Otherwise, do not determine k to run Influence Spectrum.

Dataset Format:
    The network input file is required to be in binary file which is converted from el2bin program attached.
    Network conversion:
	
	./el2bin <input text file> <binary output file>	

    The el2bin program expects two arguments:
	<input text file>: 
		the path to text file in edge list format: the first line contains the number of nodes n and number of edges m, each of the next m lines describes an edge with the format: <from> <to> <weight>

	<binary output file>: 
		the path to the binary file
   
    ***** Note: It is always a directed graph
              Node index ranges in [1 to n] (inclusive)
