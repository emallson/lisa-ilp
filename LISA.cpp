#include "IPBuilding.hpp"
#include "option.h"

int main(int argc, char ** argv)
{
  std::random_device rd;
  std::mt19937_64 mt(rd());
  std::uniform_real_distribution<double> dist(0, 1);

  IloEnv env;
  IloModel model(env);
  IloNumVarArray var(env);
  IloRangeArray con(env);

  OptionParser op(argc, argv);
  if (!op.validCheck()){
    printf("Parameters error, please check the readme.txt file for correct format!\n");
    return -1;
  }
  char * inFile = op.getPara("-i");
  if (inFile == NULL){
    inFile = (char*)"network.bin";
  }
  printf("Input file: %s\n", inFile);

  char * mo = op.getPara("-m");
  if (mo == NULL)
    mo = (char *) "LT";

  Graph g;
  if (strcmp(mo, "LT") == 0){
    g.readGraphLT(inFile);
  } else if (strcmp(mo, "IC") == 0){
    g.readGraphIC(inFile);
  } else {
    printf("Incorrect model option!");
    return -1;
  }
  char * tmp = op.getPara("-epsilon");
  float precision = 0.1;
  if (tmp != NULL){
    precision = atof(tmp);
  }

  float delta = 0.1;
  float miu = 0.1;

  tmp = op.getPara("-delta");
  if (tmp != NULL)
    delta = atof(tmp);

  tmp = op.getPara("-miu");
  if (tmp != NULL)
    miu = atof(tmp);

  int k = g.getSize();
  tmp = op.getPara("-k");
  if (tmp != NULL){
    k = atoi(tmp);
  }

  int t = 1;
  tmp = op.getPara("-t");
  if (tmp != NULL){
    t = atoi(tmp);
  }

  int n = g.getSize();
  long long m = g.getEdge();
  double totalCost = 0, totalBenefit = 0;
  for(int i = 0; i < n; i++) {
    totalCost += g.getNodeCosts()[i];
    totalBenefit += g.getNodeBenefits()[i];
  }
  printf("Number of nodes: %d\n", n);
  printf("Number of edges: %lld\n", m);
  printf("Sum of costs: %f\n", totalCost);
  printf("Sum of benefits: %f\n", totalBenefit);
  printf("Precision: %f\n", precision);
  printf("Propagation model: %s\n", mo);
  time_t time1;
  time(&time1);

  // HyperGraph hg(n);

  int search = 0;
  tmp = op.getPara("-s");
  if (tmp != NULL && strcmp(tmp, "linear") == 0){
    search = 1;
  }

  float cover = 1;
  tmp = op.getPara("-c");
  if (tmp != NULL)
    cover = atof(tmp);

  printf("Start building hypergraph!\n");
  long long counter;
  vector<float> iter_benefits;
  if (strcmp(mo, "LT") == 0){
    PollingLT p;
    iter_benefits = buildHyperGraph(g,k,precision,delta,miu,n,t,mt,dist,model,var,con,p);
  }else {
    PollingIC p;
    iter_benefits = buildHyperGraph(g,k,precision,delta,miu,n,t,mt,dist,model,var,con,p);
  }
  counter = iter_benefits.size();
  printf("Finish building hypergraph!\n");

  int timeLimit = 60*60;
  int threads = 1;
  tmp = op.getPara("-time");
  if (tmp != NULL)
    timeLimit = atoi(tmp);

  tmp = op.getPara("-threads");
  if (tmp != NULL)
    threads = atoi(tmp);

  IloCplex cplex(model);
  cplex.setParam(IloCplex::Threads, threads);
  cplex.setParam(IloCplex::Param::MIP::Strategy::File, 3);
  cplex.setParam(IloCplex::Param::WorkDir, ".");
  cplex.setParam(IloCplex::Param::Emphasis::Memory, true);
  cplex.setParam(IloCplex::Param::MIP::Limits::TreeMemory, 10000);
  cplex.setParam(IloCplex::Param::MIP::Pool::Intensity, 1);

  // Optimize the problem and obtain solution.
  cplex.solve();
  IloNumArray vals(env);
  env.out() << "Solution status = " << cplex.getStatus() << endl;
  env.out() << "Solution value  = " << cplex.getObjValue() << endl;
  env.out() << "Optimality gap = " << cplex.getMIPRelativeGap() << endl;
  cplex.getValues(vals, var);
  //env.out() << "Values        = " << vals << endl;
  ofstream outStr(op.getPara("-o"));

  env.out() << "Selected Seed Set: ";

  for (int i = 0; i < n; ++i){
    if (vals[i] == 1){
      env.out() << i + 1 << " ";
      outStr << i + 1 << " ";
    }
  }
  outStr << endl;
  env.out() << endl;

  // for(int i = 1; i <= n; ++i) {
  //   vector<int> vi = g[i];
  //   cout << i << ": ";
  //   for(int j = 0; j < g.getDegree(i); j++) {
  //     cout << vi[j] << " ";
  //   }
  //   cout << endl;
  // }

  float benefit = 0;
  for (long long i = 0; i < counter; ++i){
    if (vals[n + i] == 1) {
      benefit += iter_benefits[i];
    }
  }
  env.out() << "Influence of seed set: " << (double)benefit * n/counter << endl;
  outStr << "Influence of seed set: " << (double)benefit * n/counter << endl;
  outStr.close();
  //cplex.getSlacks(vals, con);
  //env.out() << "Slacks        = " << vals << endl;
  //cplex.getDuals(vals, con);
  //env.out() << "Duals         = " << vals << endl;
  //cplex.getReducedCosts(vals, var);
  //env.out() << "Reduced Costs = " << vals << endl;

  time_t time2;
  time(&time2);
  float min = ((float)(time2 - time1))/60;
  printf("\n\n");
  printf("Running time: %f\n\n", min);
  printf("Memory used: %f\n\n", getCurrentMemoryUsage());


  env.end();

  return 0;
}
