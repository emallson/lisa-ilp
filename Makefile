CPPFLAGS=-std=c++11 -O3 -g
CPLEX_INCLUDE=-I/opt/ibm/ILOG/CPLEX_Studio1263/cplex/include \
			  -I/opt/ibm/ILOG/CPLEX_Studio1263/concert/include
CPLEX_LIB=-L/opt/ibm/ILOG/CPLEX_Studio1263/cplex/lib/x86-64_linux/static_pic \
		  -L/opt/ibm/ILOG/CPLEX_Studio1263/concert/lib/x86-64_linux/static_pic \
		  -lilocplex -lcplex -lconcert -lm -lpthread

CPLEX_FLAGS=${CPLEX_INCLUDE} ${CPLEX_LIB} -DNDEBUG -DILOSTRICTPOD -DIL_STD

all: el2bin LISA SAA SAA_spectrum SAA-E SAA-E_spectrum

LISA: LISA.cpp option.o rwgraph.o option.h rwgraph.h IPBuilding.hpp
	g++ -static LISA.cpp rwgraph.o option.o -o $@ ${CPPFLAGS} -m64 -O \
		-fPIC -fexceptions -fopenmp ${CPLEX_FLAGS}

SAA: SAA.cpp option.o rwgraph.o option.h rwgraph.h rwgraph_algs.hpp
	g++ -static SAA.cpp rwgraph.o option.o  -o $@ ${CPPFLAGS} -m64 -O \
		-fPIC -fexceptions -fopenmp ${CPLEX_FLAGS}

SAA_spectrum: SAA_spectrum.cpp option.o rwgraph.o option.h rwgraph.h rwgraph_algs.hpp
	g++ -static SAA_spectrum.cpp rwgraph.o option.o  -o $@ ${CPPFLAGS} -m64 -O \
		-fPIC -fexceptions -fopenmp ${CPLEX_FLAGS}

SAA-E: SAA-E.cpp option.o rwgraph.o option.h rwgraph.h rwgraph_algs.hpp
	g++ -static SAA-E.cpp rwgraph.o option.o  -o $@ ${CPPFLAGS} -m64 -O \
		-fPIC -fexceptions -fopenmp ${CPLEX_FLAGS}
SAA-E_spectrum: SAA-E_spectrum.cpp option.o rwgraph.o option.h rwgraph.h rwgraph_algs.hpp
	g++ -static SAA-E_spectrum.cpp rwgraph.o option.o  -o $@ ${CPPFLAGS} -m64 -O \
		-fPIC -fexceptions -fopenmp ${CPLEX_FLAGS}
clean:
	rm el2bin LISA SAA SAA_spectrum SAA-E
	rm *.o

.PHONY: clean
