#include <cstdio>
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

int main(int argc, char ** argv)
{
  ifstream in(argv[1]);
  int n,u,v;
  long long m;
  float w;
  in >> n >> m;
  vector<int> degree(n + 1, 0);
  vector<vector<int> >eList(n + 1);
  vector<vector<float> >weight(n + 1);

  vector<float> costs(n, 0);
  vector<float> benefits(n, 0);
  float cost, benefit;
  for (int i = 0; i < n; i++) {
    in >> u >> cost >> benefit;
    costs[u-1] = cost;
    benefits[u-1] = benefit;
  }

  for (int i = 0; i < m; ++i){
    in >> u >> v >> w;
    degree[v]++;
    eList[v].push_back(u);
    weight[v].push_back(w);
  }

  in.close();

  printf("Size of graph: %i\n", degree.size());

  FILE * pFile;
  pFile = fopen(argv[2],"wb");
  fwrite(&n, sizeof(int), 1, pFile);
  fwrite(&m, sizeof(long long), 1, pFile);
  fwrite(&degree[1], sizeof(int), n, pFile);
  fwrite(&costs[0], sizeof(float),n, pFile);
  // benefits
  fwrite(&benefits[0], sizeof(float),n, pFile);

  for (int i = 1; i <= n; ++i){
    fwrite(&eList[i][0], sizeof(int), eList[i].size(), pFile);
  }

  for (int i = 1; i <= n; ++i){
    fwrite(&weight[i][0], sizeof(float), weight[i].size(), pFile);
  }


  fclose(pFile);
}
