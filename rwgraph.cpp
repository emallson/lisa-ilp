#include "rwgraph.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <queue>
#include <cstdlib>
#include <unistd.h>
#include <sstream>

using namespace std;

Graph::Graph() : numNodes(0), numEdges(0) {}

Graph::Graph(int size, int edges) : numNodes(size), numEdges(edges) {
  adjList.resize(size + 1);
  weights.resize(size + 1);
}

const vector<int> & Graph::operator [] (int u) const
{
  return adjList[u];
}

vector<int> & Graph::operator [] (int u)
{
  return adjList[u];
}

const vector<float> & Graph::getWeight (int u) const
{
  return weights[u];
}

vector<float> & Graph::getWeight (int u)
{
  return weights[u];
}

int Graph::getDegree(int u) const
{
  return adjList[u].size();
}

int Graph::getSize() const
{
  return numNodes;
}

int Graph::getEdge() const
{
  return numEdges;
}

void Graph::readGraphLT(const char* filename)
{
  FILE * pFile;
  pFile = fopen(filename, "rb");
  fread(&numNodes, sizeof(int), 1, pFile);
  fread(&numEdges, sizeof(long long), 1, pFile);
  vector<int> degree(numNodes + 1);
  fread(&degree[1], sizeof(int), numNodes, pFile);
  vector<int> a;
  vector<float> b;
  adjList.push_back(a);
  weights.push_back(b);
  vector<float> tmp1(numNodes,0);
  vector<float> tmp2(numNodes,0);
  costs = tmp1;
  benefits = tmp2;
  fread(&costs[0], sizeof(float), numNodes, pFile);
  fread(&benefits[0], sizeof(float), numNodes, pFile);

  for (int i = 1; i <= numNodes; ++i){
    vector<int> tmp(degree[i]);
    fread(&tmp[0], sizeof(int), degree[i], pFile);
    adjList.push_back(tmp);
  }
  for (int i = 1; i <= numNodes; ++i){
    vector<float> tmp(degree[i] + 1, 0);
    fread(&tmp[1], sizeof(float), degree[i], pFile);

    for(int j = 1;j < degree[i] + 1; ++j){
      tmp[j] += tmp[j-1];
    }
    weights.push_back(tmp);
  }

}

void Graph::readGraphIC(const char* filename)
{
  FILE * pFile;
  pFile = fopen(filename, "rb");
  fread(&numNodes, sizeof(int), 1, pFile); // n
  fread(&numEdges, sizeof(long long), 1, pFile); // m

  vector<int> degree(numNodes + 1);
  fread(&degree[1], sizeof(int), numNodes, pFile); // degree

  vector<int> a;
  vector<float> b;
  adjList.push_back(a);
  weights.push_back(b);
  vector<float> tmp1(numNodes,0);
  vector<float> tmp2(numNodes,0);

  costs = tmp1;
  benefits = tmp2;

  fread(&costs[0], sizeof(float), numNodes, pFile); // costs
  fread(&benefits[0], sizeof(float), numNodes, pFile); // benefits

  for (int i = 1; i <= numNodes; ++i){
    vector<int> tmp(degree[i]);
    fread(&tmp[0], sizeof(int), degree[i], pFile); // eList
    adjList.push_back(tmp);
  }

  for (int i = 1; i <= numNodes; ++i){
    vector<float> tmp(degree[i]+1, 0);
    fread(&tmp[1], sizeof(float), degree[i], pFile); // weight
    weights.push_back(tmp);
  }
}

void Graph::writeToFile(const char * filename)
{
  ofstream output(filename);
  for (int i = 0; i < numNodes; ++i){
    for (int j = 0; j < adjList[i].size(); ++j){
      if (adjList[i][j] > i){
        output << adjList[i][j] << " " << i << " " << weights[i][j] << endl;
      }
    }
  }
  output.close();
}

std::vector<float> & Graph::getNodeCosts()
{
  return costs;
}

const std::vector<float> & Graph::getNodeCosts() const
{
  return costs;
}

std::vector<float> Graph::getNodeBenefits()
{
  return benefits;
}

const std::vector<float> Graph::getNodeBenefits() const
{
  return benefits;
}

// choose a random edge in LT model based on linear search
int randIndex_lin(vector<float> &w,  std::mt19937_64 & mt, std::uniform_real_distribution<double> &dist)
{
  double ranNum = dist(mt);
  if (w.size() <= 1 || ranNum > w[w.size() - 1])
    return -1;

  for (int i = 0; i < w.size() - 1; ++i){
    if (ranNum >= w[i] && ranNum < w[i + 1])
      return i;
  }
}

// choose a random live edge in LT model based on binary search
int randIndex_bin(vector<float> &w,  std::mt19937_64 & mt, std::uniform_real_distribution<double> &dist)
{
  double ran = dist(mt);
  // printf("Draw random number: %f\n", ran);
  if (w.size() <= 1 || ran > w[w.size() - 1])
    return -1;
  int left = 1;
  int right = w.size() - 1;
  int prob;
  for (int i = 0; i < w.size(); ++i){
    prob = (left + right)/2;
    if (w[prob - 1] > ran){
      right = prob - 1;
      continue;
    }
    if (w[prob] <= ran){
      left = prob + 1;
      continue;
    }
    break;
  }
  return prob;
}

HyperGraph::HyperGraph(int n)
{
  for (int i = 0; i <=n; ++i){
    vector<int> tmp;
    node_edge.push_back(tmp);
  }
  maxDegree = 0;
  numNodes = n;
}

void HyperGraph::addEdge(vector<int> edge)
{
  edge_node.push_back(edge);
  int ind = edge_node.size() - 1;
  for (int i = 0; i < edge.size(); ++i)
    node_edge[edge[i]].push_back(ind);
}

void HyperGraph::addEdgeD(vector<int> edge)
{
  edge_node.push_back(edge);
  int ind = edge_node.size() - 1;
  for (int i = 0; i < edge.size(); ++i){
    node_edge[edge[i]].push_back(ind);
    if (node_edge[edge[i]].size() > maxDegree)
      maxDegree = node_edge[edge[i]].size();
  }
}

const vector<int> & HyperGraph::getEdge(int e) const{
  return edge_node[e];
}
const vector<int> & HyperGraph::getEdge(int e){
  return edge_node[e];
}
const vector<int> & HyperGraph::getNode(int n) const{
  return node_edge[n];
}
const vector<int> & HyperGraph::getNode(int n){
  return node_edge[n];
}

int HyperGraph::getNumEdge() const
{
  return edge_node.size();
}

int HyperGraph::getMaxDegree()
{
  return maxDegree;
}

void HyperGraph::clearEdges()
{
  edge_node.clear();
  node_edge.clear();
  for (int i = 0; i <=numNodes; ++i){
    vector<int> tmp;
    node_edge.push_back(tmp);
  }
  maxDegree = 0;
}

// polling process in LT model
void PollingLT::polling(const Graph &g, int source, vector<int> & res, std::mt19937_64 & mt, std::uniform_real_distribution<double> &dist)
{
  int i;
  int count = 0;
  int cur = source;
  int gSize = g.getSize();
  vector<bool> mark(gSize + 1, false);
  for (i = 0; i < gSize; ++i){
    if (mark[cur] == true) break;
    mark[cur] = true;
    res.push_back(cur);
    vector<float> w = g.getWeight(cur);
    vector<int> neigh = g[cur];
    int ind = randIndex_bin(w,mt,dist);
    if (ind == -1)
      break;
    cur = neigh[ind - 1];
  }
}

/*
// polling process in LT model
void pollingLT_lin(const Graph &g, int source, vector<int> & res, std::mt19937_64 & mt, std::uniform_real_distribution<double> &dist)
{
int i;
int count = 0;
int cur = source;
int gSize = g.getSize();
vector<bool> mark(gSize + 1, false);
for (i = 0; i < gSize; ++i){
if (mark[cur] == true) break;
mark[cur] = true;
res.push_back(cur);
vector<float> w = g.getWeight(cur);
vector<int> neigh = g[cur];
int ind = randIndex_lin(w,mt,dist);
if (ind == -1)
break;
cur = neigh[ind];
}
}
*/

// polling process in IC model
void PollingIC::polling(const Graph &g, int source, vector<int> & res, std::mt19937_64 & mt, std::uniform_real_distribution<double> &dist)
{
  int i;
  int count = 0;
  int cur;
  int gSize = g.getSize();
  queue<int> q;
  q.push(source);
  vector<bool> mark(gSize + 1, false);
  mark[source] = true;
  res.push_back(source);
  while(!q.empty()){
    cur = q.front();
    q.pop();
    vector<float> w = g.getWeight(cur);
    vector<int> neigh = g[cur];
    for (i = 0; i < neigh.size(); ++i){
      if (!mark[neigh[i]]){
        if (dist(mt) <  w[i+1]){
          q.push(neigh[i]);
          res.push_back(neigh[i]);
          mark[neigh[i]] = true;
        }
      }
    }
  }
}

string intToStr(int i) {
  stringstream ss;
  ss << i;
  return ss.str();
}

unsigned int strToInt(string s) {
  unsigned int i;
  istringstream myStream(s);

  if (myStream>>i) {
    return i;
  } else {
    cout << "String " << s << " is not a number." << endl;
    return atoi(s.c_str());
  }
  return i;
}

float getCurrentMemoryUsage() {

  string pid = intToStr(unsigned(getpid()));
  string outfile = "tmp_" + pid + ".txt";
  string command = "pmap " + pid + " | grep -i Total | awk '{print $2}' > " + outfile;
  system(command.c_str());

  string mem_str;
  ifstream ifs(outfile.c_str());
  std::getline(ifs, mem_str);
  ifs.close();

  mem_str = mem_str.substr(0, mem_str.size()-1);
  float mem = (float)strToInt(mem_str);

  command = "rm " + outfile;
  system(command.c_str());

  return mem/1024;

  return 0;
}
