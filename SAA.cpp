#include "option.h"
#include "rwgraph.h"
#include <iostream>
#include <random>
#include <ilcplex/ilocplex.h>
#include <vector>
#include <limits>
#include <chrono>
#include <algorithm>

#include "rwgraph_algs.hpp"

using namespace std;

int main(int argc, char** argv) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  mt = std::mt19937_64(seed);
  OptionParser opt(argc, argv);

  string graph_filename;
  int T = 0, b = 0;

  if(opt.getPara("-i") != NULL) {
    graph_filename = opt.getPara("-i");
  } else {
    cerr << "No input file (-i) provided." << endl;
    return -1;
  }

  if(opt.getPara("-T") != NULL) {
    T = stoi(opt.getPara("-T"));
  }

  // budget parameter
  if(opt.getPara("-b") != NULL) {
    b = stoi(opt.getPara("-b"));
  }

  cerr << graph_filename << " " << T << " " << b << endl;

  // read the graph in
  Graph g;
  g.readGraphIC(graph_filename.c_str());

  int n = g.getSize();

  auto start_time = std::chrono::system_clock::now();

  cerr << "Realizing graphs... ";
  // realize T deterministic graphs
  vector<Graph> E;
  vector<vector<vector<int>>> dists;
  for(int i = 0; i < T; i++) {
    cerr << i << " ";
    E.push_back(forward_realize(g));
    dists.push_back(all_pairs_shortest_path(E[i]));
  }
  cerr << endl;

  IloEnv env;
  IloModel model(env);
  IloBoolVarArray x(env, T * n), s(env, n);

  cerr << "Constructing budget constraint..." << endl;
  // constrain the seeds selected to remain within the budget b
  IloExpr seed_cost(env, 0);
  const vector<float> costs = g.getNodeCosts();
  for(int v = 1; v <= n; v++) {
    seed_cost += s[v-1] * costs[v-1];
  }
  model.add(seed_cost <= b);

  cerr << "Constructing reachability constraints..." << endl;
  // constrain the x's based on reachability
  for(int l = 0; l < T; l++) {
    for(int v = 1; v <= n; v++) {
      IloExpr seeds_reachable(env, 0);
      for(int u = 1; u <= n; u++) {
        if(reachable(dists[l], u, v)) {
          // cerr << u << " " << v << endl;
          seeds_reachable += s[u-1];
        }
      }
      model.add(seeds_reachable >= x[l * n + v-1]);
    }
  }

  // maximize the benefit
  const vector<float> benefits = g.getNodeBenefits();
  IloExpr benefit(env, 0);
  for(int i = 0; i < T * n; i++) {
    benefit += x[i] * benefits[i % n];
  }
  benefit /= T;
  model.add(IloMaximize(env, benefit));

  int timeLimit = 120 * 60;
  int threads = 1;

  cerr << "Optimizing..." << endl;
  IloCplex cplex(model);
  cplex.setParam(IloCplex::Threads, threads);

  cplex.solve();

  auto end_time = std::chrono::system_clock::now();
  IloNumArray selected_seeds(env);

  env.out() << "Solution status = " << cplex.getStatus() << endl;
  env.out() << "Solution value  = " << cplex.getObjValue() << endl;
  env.out() << "Optimality gap = " << cplex.getMIPRelativeGap() << endl;
  cplex.getValues(selected_seeds, s);

  env.out() << "Selected Seed Set: ";
  for(int i = 0; i < n; ++i) {
    if(selected_seeds[i] == 1) {
      env.out() << i+1 << " ";
    }
  }
  env.out() << endl;

  env.out() << "Influence of seed set: " << cplex.getObjValue() << endl;

  std::chrono::duration<double> runtime = end_time - start_time;
  env.out() << "Running time: " << runtime.count() << endl;

  return 0;
}
