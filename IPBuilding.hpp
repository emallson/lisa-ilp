#ifndef _IPBUILDING_H_
#define _IPBUILDING_H_

#include "rwgraph.h"
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <omp.h>
#include <cmath>
#include <vector>
#include <fstream>
#include <cstring>
#include <random>
#include <ilcplex/ilocplex.h>

ILOSTLBEGIN

using namespace std;

template<class PollingProcess>
vector<float> buildHyperGraph(Graph & g, int k, float precision, float delta, float miu, int n, int t, std::mt19937_64 & mt, std::uniform_real_distribution<double> & dist, IloModel model, IloNumVarArray x, IloRangeArray co, PollingProcess & p)
{

  vector<float> bTmp = g.getNodeBenefits();
  for (int i = 1; i < n; ++i){
//		printf("%f ", bTmp[i -1]);
    bTmp[i] += bTmp[i-1];
  }

//	printf("\nBenefit distribution: ");
  vector<float> bDist(n + 1, 0);
  for (int i = 0; i < n; ++i){
    bDist[i+1] = bTmp[i]/bTmp[n-1];
//		printf("%f ", bDist[i+1]);
  }
  printf("\n");

  printf("Number of allocated threads\n");
  int c = 100;
  int iter = 0;
  // temporary array for holding hyperedges before put them into the hypergraph
  vector<vector<vector<int> > >tmp(t);

  std::uniform_int_distribution<int> dis(1,n);

  int numNodes = g.getSize();

  float rho = miu*(1-miu);
  rho *= rho;
  if (rho < precision*miu)
    rho = precision*miu;
  // the largest node degree needed to guarantee the approximation factor
  double upperBound = 2*2*(exp(1) - 2)*(log(2/delta) + k * log((float)n/(float)k))*rho/(precision*precision*miu*miu);

  // double upperBound = 1+(1+precision)*(log(2) + log(n))*4*(exp(1) - 2)/(precision*precision);
  printf("Number of allocated threads: %d\n", t);
  // max node degree in the hypergraph
  printf("Upper bound for degree: %f\n", upperBound);
  omp_set_num_threads(t);
  printf("Build HyperGraph.\n");

  long long counter = 0;

  vector<float> benefits = g.getNodeBenefits();
  vector<float> iter_benefits;
  iter_benefits.reserve(ceil(upperBound));
  printf("Build hyperedge using binary search!\n");
#pragma omp parallel
  {
    while (iter < upperBound){
      // printf("Max Degree %d\n", maxDegG);
      int id = omp_get_thread_num();
      int ver;

      for (int i = 0; i < c; ++i){
        vector<int> he;
        ver = randIndex_bin(bDist,mt,dist);
        // generate a hyperedge and store in he variable
        p.polling(g,ver,he,mt,dist);
        for(int q : he) {
        }
        tmp[id].push_back(he);
        //printf("\n");
#pragma omp critical (benefit_push)
        iter_benefits.push_back(benefits[ver-1]);
      }
      //printf("Sync done %d !\n", tmp[id].size());
#pragma omp atomic
      iter += c;
    }
  }

  IloEnv env = model.getEnv();
  // add variables into the var container
  for (int i = 0; i < numNodes + iter; ++i){
    x.add(IloBoolVar(env));
  }

  vector<float> costs = g.getNodeCosts();

  long long count = 0;

  // add hyperedges to hypergraph
  for (int i = 0; i < t; ++i){
    for (long long j = 0; j < tmp[i].size(); ++j){
      IloExpr constraint(env);
      for (int ij = 0; ij < tmp[i][j].size(); ij++){
        constraint += x[tmp[i][j][ij] - 1];
      }
      constraint -= x[count + numNodes];
      co.add(IloRange(env, 0, constraint));

      count++;
    }
    // clean memory after taking all the edges
    vector<vector<int> >().swap(tmp[i]);
  }
  // required influence

  IloExpr ben(env, 0);
  for (int i = 0; i < iter; ++i){
    ben += x[i + numNodes] * iter_benefits[i];
  }

  // double requiredInf = iter*miu;
  // printf("Required Influence: %f\n", requiredInf);
  // co.add(IloRange(env, requiredInf, inf));

  // IloExpr ob(env);
  // for (int i = 0; i < numNodes; ++i)
  //   ob += costs[i]*x[i];

  // model.add(IloMinimize(env,ob));
  model.add(IloMaximize(env,ben));
  model.add(co);

  IloExpr kc(env, 0);
  for(int i = 0; i < numNodes; ++i) {
    kc += x[i] * costs[i];
  }
  model.add(kc <= k);
  return iter_benefits;
}

#endif
