#include "option.h"
#include "rwgraph.h"
#include <iostream>
#include <random>
#include <ilcplex/ilocplex.h>
#include <vector>
#include <set>
#include <stack>
#include <limits>
#include <chrono>
#include <algorithm>
#include <unordered_map>

#include "rwgraph_algs.hpp"

using namespace std;

void _cycle_dfs(const Graph& g, int s,
                unordered_map< int, unordered_map<int, bool> >& active,
                vector<bool>& marked, vector<bool>& onStack, vector<int>& cycle) {
  marked[s] = true;
  onStack[s] = true;
  for(int w : g[s]) {
    if(active[s][w] == 0) {
      continue;
    }

    if(!marked[w]) {
      _cycle_dfs(g, w, active, marked, onStack, cycle);
      if(cycle.size() > 0) {
        if(cycle.size() > 1 && cycle.front() == cycle.back()) {
          // we found a complete cycle from a to a that is non-trivial
          break;
        } else {
          cycle.push_back(w);
          break;
        }
      }
    } else if(onStack[w]) {
      cycle.push_back(w);
      break;
    }
  }

  onStack[s] = false;
}

vector<int> cycle_dfs(const Graph& g, int s, unordered_map< int, unordered_map<int, bool> >& active) {
  vector<int> cycle;
  vector<bool> marked(g.getSize() +1, false),
    onStack(g.getSize() + 1, false);

  _cycle_dfs(g, s, active, marked, onStack, cycle);

  if(cycle.size() > 0 && cycle.front() != cycle.back()) {
    cycle.push_back(s);
  }

  return cycle;
}

bool add_cycle_constraint(IloModel& model, IloEnv& env,
                          IloBoolVarArray& y, IloNumArray& active_edges,
                          int& T, vector<Graph>& E,
                          unordered_map< int, unordered_map<int, int> >& edge_nums) {
  unordered_map< int, unordered_map<int, bool> > active;

  int m = E[0].getEdge();       // getEdge is not changed when edges
                                // are deleted

  for(int l = 0; l < T; l++) {
    int n = E[l].getSize();
    active.clear();
    for(int v = 1; v <= n; v++) {
      for(int u : E[l][v]) {
        active[v][u] = active_edges[m * l + edge_nums.at(v).at(u)];
      }
    }

    vector<int> cycle;
    for(int v = 1; v <= n; v++) {
      cycle = cycle_dfs(E[l], v, active);
      if(cycle.size() > 0) {
        cerr << "Found cycle on " << l << "...";
        for(int w : cycle) {
          cerr << w << " ";
        }
        cerr << endl;
        IloExpr cycle_edges(env, 0);
        for(int i = 1; i < cycle.size(); i++) {
          cerr << active.at(cycle[i]).at(cycle[i-1]) << " ";
          cycle_edges += y[m * l + edge_nums.at(cycle[i]).at(cycle[i-1])];
        }
        cerr << endl;

        // there are cycle.size() - 1 edges in the cycle
        model.add(cycle_edges < (int)(cycle.size() - 1));
        return true;
      }
    }
  }
  return false;
}

ILOLAZYCONSTRAINTCALLBACK4(lazy_cycle_constraint,
                           IloBoolVarArray&, y,
                           int&, T, vector<Graph>&, E,
                           edge_map<int>&, edge_nums) {
  cerr << "Entering cycle constraint callback..." << endl;
  edge_map<bool> active;
  IloNumArray active_edges(this->getEnv());
  this->getValues(active_edges, y);
  int m = E[0].getEdge();       // getEdge is not changed when edges
                                // are deleted

  for(int l = 0; l < T; l++) {
    int n = E[l].getSize();
    active.clear();
    for(int v = 1; v <= n; v++) {
      for(int u : E[l][v]) {
        active[v][u] = active_edges[m * l + edge_nums.at(v).at(u)];
      }
    }
    cerr << "Looking for cycles...";
    vector< vector<int> > cycles = directed_circuits(E[l], active);
    cerr << "Found " << cycles.size() << " cycles on " << l << endl;
    for(vector<int> cycle : cycles) {
      try {
        IloExpr cycle_edges(this->getEnv(), 0);
        for(int i = 1; i < cycle.size(); i++) {
          cycle_edges += y[m * l + edge_nums.at(cycle[i-1]).at(cycle[i])];
        }

        // there are cycle.size() - 1 edges in the cycle
        this->add(cycle_edges < (int)(cycle.size() - 1));
      } catch(IloCplex::Exception e) {
        cerr << e << endl;
        throw e;
      }
    }
  }
}

int main(int argc, char** argv) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  mt = std::mt19937_64(seed);
  OptionParser opt(argc, argv);

  string graph_filename;
  int T = 0, b = 0;

  if(opt.getPara("-i") != NULL) {
    graph_filename = opt.getPara("-i");
  } else {
    cerr << "No input file (-i) provided." << endl;
    return -1;
  }

  if(opt.getPara("-T") != NULL) {
    T = stoi(opt.getPara("-T"));
  }

  // budget parameter
  if(opt.getPara("-b") != NULL) {
    b = stoi(opt.getPara("-b"));
  }

  cerr << graph_filename << " " << T << " " << b << endl;

  // read the graph in
  Graph g;
  g.readGraphIC(graph_filename.c_str());

  int n = g.getSize(),
    m = g.getEdge();

  unordered_map< int, unordered_map<int, int> > edge_nums;
  int index = 0;
  for(int u = 1; u <= n; u++) {
    for(int v : g[u]) {
      edge_nums[u][v] = index++;
    }
  }

  auto start_time = std::chrono::system_clock::now();

  cerr << "Realizing graphs... ";
  // realize T deterministic graphs
  vector<Graph> E;
  for(int i = 0; i < T; i++) {
    cerr << i << " ";
    E.push_back(forward_realize(g));
  }
  cerr << endl;

  IloEnv env;
  IloModel model(env);
  IloBoolVarArray y(env, T * m), s(env, n);

  cerr << "Constructing budget constraint..." << endl;
  // constrain the seeds selected to remain within the budget b
  IloExpr seed_cost(env, 0);
  const vector<float> costs = g.getNodeCosts();
  for(int v = 1; v <= n; v++) {
    seed_cost += s[v-1] * costs[v-1];
  }

  cerr << "Constructing edge constraints..." << endl;
  for(int l = 0; l < T; l ++) {
    for(int v = 1; v <= n; v++) {
      IloExpr vsum(env, 0);
      for(int u : g[v]) {
        IloExpr sum(env, 0);
        for(int w : g[u]) {
          sum += y[m * l + edge_nums.at(u).at(w)];
        }
        sum += s[u-1];
        model.add(sum >= y[m * l + edge_nums.at(v).at(u)]);

        vsum += y[m * l + edge_nums.at(v).at(u)];
      }
      model.add(vsum <= 1);
    }
  }

  cerr << "Constructing trivial cycle constraints..." << endl;
  for(int v = 1; v <= n; v++) {
    for(int u : g[v]) {
      if(edge_nums.count(u) && edge_nums.at(u).count(v) > 0) {
        // there are edges u -> v and v -> u
        for(int l = 0; l < T; l++) {
          IloExpr sum(env, 0);
          sum += y[m * l + edge_nums.at(u).at(v)] + y[m * l + edge_nums.at(v).at(u)];
          model.add(sum < 2);
        }
      }
    }
  }

  cerr << "Constructing objective..." << endl;
  IloExpr influence(env, 0);
  for(int i = 0; i < T * m; i++) {
    influence += y[i];
  }
  influence /= T;
  for(int i = 0; i < n; i++) {
    influence += s[i];
  }

  model.add(IloMaximize(env, influence));

  int timeLimit = 30 * 60;
  int threads = 1;

  cerr << "Optimizing..." << endl;
  IloCplex cplex(model);
  cplex.setParam(IloCplex::Threads, threads);
  cplex.setParam(IloCplex::Param::MIP::Strategy::File, 3);
  cplex.setParam(IloCplex::Param::WorkDir, ".");
  cplex.setParam(IloCplex::Param::Emphasis::Memory, true);
  cplex.setParam(IloCplex::Param::MIP::Limits::TreeMemory, 10000);
  //cplex.setParam(IloCplex::Param::MIP::Pool::Intensity, 1);
  cplex.use(lazy_cycle_constraint(env, y, T, E, edge_nums));
  for(int i = 1; i <= b; i += b/10) {
    auto budget_constraint = seed_cost <= i;
    model.add(budget_constraint);
    cplex.solve();

    auto end_time = std::chrono::system_clock::now();
    IloNumArray selected_seeds(env);

    env.out() << "Solution status = " << cplex.getStatus() << endl;
    env.out() << "Solution value  = " << cplex.getObjValue() << endl;
    env.out() << "Optimality gap = " << cplex.getMIPRelativeGap() << endl;
    cplex.getValues(selected_seeds, s);

    env.out() << "Selected Seed Set: ";
    for(int i = 0; i < n; ++i) {
      if(selected_seeds[i] == 1) {
        env.out() << i+1 << " ";
      }
    }
    env.out() << endl;

    env.out() << "Influence of seed set: " << cplex.getObjValue() << endl;

    std::chrono::duration<double> runtime = end_time - start_time;
    env.out() << "Running time: " << runtime.count() << endl;

    model.remove(budget_constraint);
    if(i == 1) {
      i = 0;
    }
  }

  return 0;
  return 0;
}
