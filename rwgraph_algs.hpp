// no include guard because double-includes of this SHOULD throw
// compile errors.
//
// hacks like this deserve to cause compile errors.
#include "rwgraph.h"
#include <iostream>
#include <random>
#include <ilcplex/ilocplex.h>
#include <vector>
#include <set>
#include <stack>
#include <limits>
#include <chrono>
#include <algorithm>
#include <unordered_map>

using namespace std;

template <typename T> using edge_map = unordered_map<int, unordered_map<int, T> >;

mt19937_64 mt;
uniform_real_distribution<double> uniform(0.0, 1.0);

#define IINF numeric_limits<int>::max() - 1

void _strongconnect(vector< set<int> >& output,
                    vector<int>& ind, vector<int>& lowlink,
                    vector<bool>& onStack, stack<int>& S,
                    const Graph& g, int& index, int& v, edge_map<bool>& active) {
  ind[v] = index;
  lowlink[v] = index;
  index += 1;

  S.push(v);
  onStack[v] = true;

  for(int w : g[v]) {
    if(active.at(v).at(w) == 0) {
      continue;
    }
    if(ind[w] == IINF) {
      _strongconnect(output, ind, lowlink, onStack, S, g, index, w, active);
      lowlink[v] = min(lowlink[v], lowlink[w]);
    } else if (onStack[w]) {
      lowlink[v] = min(lowlink[v], ind[w]);
    }
  }

  if(lowlink[v] == ind[v]) {
    set<int> scc;
    int w;
    do {
      w = S.top();
      S.pop();
      onStack[w] = false;
      scc.insert(w);
    } while(w != v);
    output.push_back(scc);
  }
}

// Tarjan's -- directly implemented from wikipedia
vector< set<int> > strongly_connected_components(const Graph& g, edge_map<bool>& active) {
  int n = g.getSize();
  vector< set<int> > output;
  vector<int> ind(n+1, IINF);
  vector<int> lowlink(n+1, IINF);
  vector<bool> onStack(n+1, false);

  int index = 0;
  stack<int> S;

  for(int v = 1; v <= g.getSize(); v++) {
    if(ind[v] == IINF) {
      _strongconnect(output, ind, lowlink, onStack, S, g, index, v, active);
    }
  }

  return output;
}

void _unblock(vector< set<int> > & B, vector<bool>& blocked, int& u) {
  blocked[u] = false;
  for(int i = 0; i < B[u].size();) {
    int w = *B[u].begin();
    B[u].erase(B[u].begin());
    if(blocked[w]) {
      _unblock(B, blocked, w);
    }
  }
}

bool _circuit(vector<int>& stk, vector< vector<int> >& output,
              const Graph& g, set<int>& scc, vector< set<int> >& B, vector<bool>& blocked,
              int& s, int& v, edge_map<bool>& active) {
  bool f = false;
  stk.push_back(v);
  blocked[v] = true;
  for(int w : g[v]) {
    if(active.at(v).at(w) == 0) {
      continue;                 // edge not active, skip
    }
    if(scc.count(w) == 0) {
      continue;                 // not in scc, skip it
    }
    if(w == s) {
      // output stack . s as a circuit
      output.push_back(vector<int>(stk)); // omit the second instance of s to make a *cycle set*
      f = true;
    } else if(!blocked[w]) {
      if (_circuit(stk, output, g, scc, B, blocked, s, w, active)) {
        f = true;
      }
    }
  }
  if(f) {
    _unblock(B, blocked, v);
  } else {
    for(int w : g[v]) {
      if(scc.count(w) == 0) {
        continue;                 // not in scc, skip it
      }
      B[w].insert(v);
    }
  }
  stk.pop_back();
  return f;
}

// Johnson, D. B. (1975). Finding all the elementary circuits of a
// directed graph. SIAM Journal on Computing, 4(1), 77-84.
//
// Complexity: O((n + m)(c + 1)) where c is the number of cycles.
vector< vector<int> > directed_circuits(const Graph& g, edge_map<bool>& active) {
  int n = g.getSize(), s = 1;
  vector<bool> blocked(n+1, false);
  vector< set<int> > B(n+1);
  vector< vector<int> > output;
  vector<int> stk;

  vector< set<int> > sccs = strongly_connected_components(g, active);
  sort(sccs.begin(), sccs.end(),
       [](const set<int>& a, const set<int>& b) { return *(a.begin()) < *(b.begin()); });
  int k = 0;

  while(s < n) {
    while(k < sccs.size() && *sccs[k].begin() < s) {
      k += 1;
    }

    if(k >= sccs.size()) {
      break;
    }

    s = *sccs[k].begin();
    for(int i : sccs[k]) {
      blocked[i] = false;
      B[i].clear();
    }
    _circuit(stk, output, g, sccs[k], B, blocked, s, s, active);
    s += 1;
  }

  return output;
}

vector<int> shortest_paths(const Graph& g, int v) {
  int n = g.getSize();
  vector<int> dist(n, IINF);
  vector<int> queue;
  queue.reserve(n);
  auto comp = [&dist](const int &u, const int &v) -> bool { return dist[v-1] < dist[u-1]; };

  dist[v-1] = 0;

  // initialize the queue
  for(int u = 1; u <= n; u++) {
    queue.push_back(u);
    push_heap(queue.begin(), queue.end(), comp);
  }

  while(queue.size() != 0) {
    pop_heap(queue.begin(), queue.end(), comp);
    int s = queue.back();
    queue.pop_back();

    if(dist[s-1] == IINF) {
      break;                    // early termination: if we pop an
                                // element with infinite distance, no
                                // more elements are reachable
    }

    for(int i = 0; i < g[s].size(); i++) {
      int t = g[s][i];
      int alt = dist[s-1] + 1;
      if(alt < dist[t-1]) {
        dist[t-1] = alt;
        make_heap(queue.begin(), queue.end(), comp);
      }
    }
  }

  return dist;
}

// returns the distances of between all nodes. dist[v][u] = distance
// from u to v
vector< vector<int> > all_pairs_shortest_path(const Graph& g) {
  int n = g.getSize();
  vector< vector<int> > dist;
  dist.reserve(n);

  for(int v = 1; v <= n; v++) {
    dist.push_back(shortest_paths(g, v));
  }

  return dist;
}

// returns true if v can be reached from node u
bool reachable(vector< vector<int> >& dist, int u, int v) {
  return dist[v-1][u-1] < IINF;
}

// no longer needed -- maybe?
// Graph invert(const Graph& orig) {
//   Graph copy(orig.getSize(), orig.getEdge());   // empty graph to set things to
//   copy.getNodeCosts() = orig.getNodeCosts();
//   copy.getNodeBenefits() = orig.getNodeBenefits();

//   for(int v = 1; v <= copy.getSize(); v++) {
//     auto w = orig.getWeight(v);
//     for(int i = 0; i < orig[v].size(); i++) {
//       copy[orig[v][i]].push_back(v);
//       copy.getWeight(orig[v][i]).push_back(w[i]);
//     }
//   }

//   return copy;
// }

Graph forward_realize(const Graph& orig) {
  Graph copy(orig);
  for(int i = 1; i <= copy.getSize(); i++) {
    vector<float> w = copy.getWeight(i);
    int j = 0;
    while(j < copy[i].size()) {
      if(uniform(mt) <= w[j+1]) {
        j++;
      } else {
        copy[i].erase(copy[i].begin() + j);
        // w.erase(w.begin() + j);
        copy.getWeight(i).erase(copy.getWeight(i).begin() + j+1);
      }
    }
  }
  return copy;
}
